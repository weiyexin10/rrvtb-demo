#include "dromajo.hpp"
#include "tage.hpp"
#include "rrvtb/rrvtb.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <pthread.h>
#include <queue>
using namespace std;

struct insn_t
{
    uint32_t    data;
    bool        is_br;
    bool        is_rvc;
    uint64_t    pc;
    uint64_t    btq_idx;
    bool        pred_taken;
    uint64_t    pred_target;
    bool        is_cacheline_tail;  
    bool        is_mispred;
    bool        valid;
};

Tage *bpu;
Dromajo *core;
pthread_t model_thread;

// for simulation controlling and display
uint64_t insn_cnt = 0;
uint64_t jal_cnt = 0, jalr_cnt = 0, branch_cnt = 0, other_cnt = 0;
uint64_t mispred_jal_cnt = 0, mispred_jalr_cnt = 0, mispred_branch_cnt = 0, mispred_other_cnt = 0;
uint64_t br_cnt = 0;
void statistic(const Dromajo &m, uint64_t &jal, uint64_t &jalr, uint64_t &branch, uint64_t &other)
{
    if (m.get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH) branch++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_CALL) jalr++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_JALR) jalr++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_RET) jalr++;
    else if (m.get_last_insn_cfi_type() == Dromajo::TYPE_JAL) jal++;
    else other++;
}

void *model_fn(void *arg){
    bool keep_going = true;
    uint64_t curr_pc;
    bool last_is_br, last_is_taken, last_mispred;
    TageMeta last_meta;
    bpu->Reset();
    while (true)
    {
        curr_pc = core->get_pc();
        last_is_br = core->get_last_insn_cfi_type() == Dromajo::TYPE_BRANCH;
        last_is_taken = core->last_insn_is_taken();
        last_mispred = last_is_taken != last_meta.pred;
        bpu->Evaluate(curr_pc, last_is_br, last_is_taken, last_meta);

        statistic(*core, jal_cnt, jalr_cnt, branch_cnt, other_cnt);
        if (last_is_br && last_mispred)
        {
            statistic(*core, mispred_jal_cnt, mispred_jalr_cnt, mispred_branch_cnt, mispred_other_cnt);
        }
        bpu->Advance();   
        keep_going = core->iterate();
        if (!keep_going)
        {
            int exitcode = core->get_exit_code();
            INFO("EXIT! code = %d\n", exitcode);
            INFO("==================statistic==================\n");
            INFO("total: %d, branch: %d, jalr: %d, jal: %d (other: %d)\n",
                branch_cnt + jalr_cnt + jal_cnt, branch_cnt, jalr_cnt, jal_cnt, other_cnt);
            INFO("mis_total: %d, mis_branch: %d, mis_jalr: %d, mis_jal: %d, (mis_other: %d)\n",
                mispred_branch_cnt + mispred_jalr_cnt + mispred_jal_cnt, 
                mispred_branch_cnt, mispred_jalr_cnt, mispred_jal_cnt, mispred_other_cnt);
            INFO("mispred rate: %f\n", 
                (double)(mispred_branch_cnt + mispred_jalr_cnt + mispred_jal_cnt)/(double)(branch_cnt + jalr_cnt + jal_cnt));
            
            finish();
        }
    }
	return((void *)0);
}


initial(initial_func)
{
    char *file_name;
    if (!(file_name = (char*)scan_plusargs("image=")))
    {
        ERROR("no image input!\n");
    }
    core = new Dromajo(file_name, 0x80000000);
    bpu = new Tage();

    int err = pthread_create(&model_thread, NULL, &model_fn, NULL);
	if (err != 0)
		ERROR("Can't create model thread\n");
    sleep(0);
}

