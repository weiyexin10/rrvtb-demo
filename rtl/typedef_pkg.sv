`include "define.sv"
package typedef_pkg;

typedef struct packed {
    logic [`CTR_LENGTH-1:0] ctr;
    logic [`TAG_LENGTH-1:0] tag;
    logic [`USEFUL_BITS_LENGTH-1:0] useful_bits;
} pred_table_entry_t;

typedef struct packed {
    logic [`TAG_LENGTH-1:0] hist;
    logic [8:0]                 ori_len;
    logic [8:0]                 comp_len;
    logic [8:0]                 outpoint;
} tag_comp_hist_t;

typedef struct packed {
    logic [`ENTRYS_LOG-1:0] hist;
    logic [8:0]                 ori_len;
    logic [8:0]                 comp_len;
    logic [8:0]                 outpoint;
} index_comp_hist_t;

function [`ENTRYS_LOG-1:0] get_idx (
    input [`PHR_LENTH-1:0] phist,
    input [`ENTRYS_LOG-1:0] index_comp_hist,
    input [`VADDR_WIDTH-1:0] pc,
    input [31:0]            hist_len,
    input [$clog2(`N_TABLES)-1:0] tables_no
    );
    
    logic [`PHR_LENTH-1:0] ex_phist, tmp_phist1, tmp_phist2, tmp_phist3, tmp_phist4, tmp_phist5;
    logic [`PHR_LENTH-1:0]           fold_mask;
    begin
        if (hist_len >= `PHR_LENTH) begin
            fold_mask = {`PHR_LENTH{1'b1}};
        end
        else begin
            for (int i=0; i<hist_len; i++) begin
                fold_mask[i] = 1'b1;
            end
        end
        // 1. mix path history: PHR_LENTH bits phist --> HIST_LENGTH bits ex_phist
        tmp_phist1 = phist & fold_mask;
        tmp_phist2 = tmp_phist1 & {`ENTRYS_LOG{1'b1}};
        tmp_phist3 = tmp_phist1 >> `ENTRYS_LOG;
        tmp_phist4 = ((tmp_phist3 << tables_no) & {`ENTRYS_LOG{1'b1}}) + (tmp_phist3 >> (`ENTRYS_LOG - tables_no));
        tmp_phist5 = tmp_phist2 ^ tmp_phist4;
        ex_phist = ((tmp_phist5 << tables_no) & {`ENTRYS_LOG{1'b1}}) + (tmp_phist5 >> (`ENTRYS_LOG - tables_no));

        // 2. fold history reg
        get_idx = pc ^ (pc >> (`ENTRYS_LOG - `N_TABLES + tables_no + 1)) ^ index_comp_hist ^ ex_phist;
    end
endfunction

function [`ENTRYS_LOG-1:0] comp_hist (
    input index_comp_hist_t ori_inx_comp,
    input [129:0]            next_ghist
    );
    logic [`ENTRYS_LOG-1:0] tmp_hist0, tmp_hist1, tmp_hist2;
    begin
        tmp_hist0 = (ori_inx_comp.hist << 1) | next_ghist[0];
        tmp_hist1 = tmp_hist0 ^ (next_ghist[ori_inx_comp.ori_len] << ori_inx_comp.outpoint);
        tmp_hist2 = tmp_hist1 ^ (tmp_hist1 >> ori_inx_comp.comp_len);
        comp_hist = tmp_hist2 & ((1 << ori_inx_comp.comp_len) - 1);
    end
endfunction

endpackage