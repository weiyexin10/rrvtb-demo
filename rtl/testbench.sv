`include "define.sv"
module testbench
import typedef_pkg::*;
(
);

parameter int unsigned HIST_LEN[`N_TABLES] = '{5, 9, 15, 25, 44, 76, 130};
parameter int unsigned TAG_LEN[`N_TABLES] = '{`TAG_LENGTH-3, `TAG_LENGTH-3, `TAG_LENGTH-2, `TAG_LENGTH-2, `TAG_LENGTH-1, `TAG_LENGTH-1, `TAG_LENGTH};

logic   clk;
logic   rst;

// generate clk
initial begin
    clk = 0;
    forever begin
        #5;
        clk = 1;
        #5;
        clk = 0;
    end
end

logic [`VADDR_WIDTH-1:0]        pc_i;
logic                           pred_o;
logic                           pred_by_table_o;
logic [$clog2(`N_TABLES)-1:0]   provider_no_o;
logic                           alt_pred_o;

logic                           update_valid_i;
logic                           taken_i;
logic                           pred_by_table_i;
logic [$clog2(`N_TABLES)-1:0]   provider_no_i;
logic                           alt_pred_i;

tage tage_u
(
    .fetch_pc_i(pc_i),
    .fetch_valid_i(1'b1),
    .fetch_pred_valid_o(),
    .fetch_pred_by_table_o(pred_by_table),
    .provider_no_o(provider_no),
    .fetch_pred_o(pred_o),
    .fetch_alt_pred_o(alt_pred),

    .update_vaild_i(update_valid_i),
    .update_pc_i(pc_i),
    .update_br_result_i(taken_i),
    .update_pred_i(pred_o),
    .update_pred_by_table_i(pred_by_table_i),
    .update_alt_pred_i(alt_pred_i),
    .provider_no_i(provider_no_i),    

    .ghist(),
    .phist(),
    .tag0_comp_hist_vec(),
    .tag1_comp_hist_vec(),
    .index_comp_hist_vec(),

    .clk(clk),
    .rst(rst)
);








endmodule
