module bht
(
    // do prediction
    input logic [`BHT_IDX_WIDTH-1:0] fetch_index_i,
    output logic prediction_o,

    // do update
    input logic update_en_i,
    input logic [`BHT_IDX_WIDTH-1:0] update_index_i,
    input logic update_br_result_i, 

    input logic rst,
    input logic clk
);

// BHT data
logic [1:0] bht_data [2**`BHT_IDX_WIDTH-1:0];

// Initialize data to 00 indicating a strong predict not taken entry
initial begin
    
end

always_ff @(posedge clk) begin
    if (rst) begin
        for (int i = 0; i < 2**`BHT_IDX_WIDTH; i++) begin
            bht_data[i] = 2'b0;
        end
    end
    else begin
        // Update previous entry based on prediction results
        if (update_en_i) begin
            if(update_br_result_i && (bht_data[update_index_i] != 2'b11))
                bht_data[update_index_i] <= bht_data[update_index_i] + 1;
            else if(~update_br_result_i && (bht_data[update_index_i] != 2'b0))
                bht_data[update_index_i] <= bht_data[update_index_i] - 1;
        end
    end
end

// Output prediction for current entry
assign prediction_o = bht_data[fetch_index_i][1];

endmodule
