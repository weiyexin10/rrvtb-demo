`include "define.sv"
module ram #(    
    parameter MEM_DEPTH  = 128,
    parameter DATA_WIDTH = 256,
    parameter ADDR_WIDTH = $clog2(MEM_DEPTH),
    parameter N_PORTS = 1
)
(
    input  logic [N_PORTS-1:0]                 en, 
    input  logic [N_PORTS-1:0]                 rw, // rw=0 (read), rw=1 (write)
    input  logic [N_PORTS-1:0][ADDR_WIDTH-1:0] addr, 
    input  logic [N_PORTS-1:0][DATA_WIDTH-1:0] din, 
    output logic [N_PORTS-1:0][DATA_WIDTH-1:0] qout,

    input  logic                  clk, 
    input  logic                  rst
);
logic [MEM_DEPTH-1:0][DATA_WIDTH-1:0] memory;

always_ff @(posedge clk) begin
    if (rst) begin
        for (int i = 0; i < MEM_DEPTH; i++) begin
            memory[i] <= '0;
        end
    end 
    else begin
        for (int i=0; i<N_PORTS; i++) begin
            if (rw[i] & en[i]) begin
                memory[addr[i]] <= din[i];
            end
            qout[i] <= memory[addr[i]];
        end
    end
end

endmodule

