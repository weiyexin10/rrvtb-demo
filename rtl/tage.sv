`include "define.sv"
module tage
import typedef_pkg::*;
#(    
    parameter int unsigned HIST_LEN[`N_TABLES] = '{5, 9, 15, 25, 44, 76, 130},
    parameter int unsigned TAG_LEN[`N_TABLES] = '{`TAG_LENGTH-3, `TAG_LENGTH-3, `TAG_LENGTH-2, `TAG_LENGTH-2, `TAG_LENGTH-1, `TAG_LENGTH-1, `TAG_LENGTH}
)
(
    input logic [`VADDR_WIDTH-1:0]  fetch_pc_i,
    input logic                     fetch_valid_i,
    output logic                    fetch_pred_valid_o,
    output logic                    fetch_pred_by_table_o,
    output logic [$clog2(`N_TABLES)-1:0] provider_no_o,  
    output logic                     fetch_pred_o,
    output logic                     fetch_alt_pred_o,

    input logic                     update_vaild_i,
    input logic [`VADDR_WIDTH-1:0]  update_pc_i,
    input logic                     update_br_result_i,
    input logic                     update_pred_i,
    input logic                     update_pred_by_table_i,
    input logic                     update_alt_pred_i,
    input logic [$clog2(`N_TABLES)-1:0] provider_no_i,    

    output logic [HIST_LEN[`N_TABLES-1]-1:0]    ghist,
    output logic [`PHR_LENTH-1:0]               phist,
    output tag_comp_hist_t [`N_TABLES-1:0]      tag0_comp_hist_vec,
    output tag_comp_hist_t [`N_TABLES-1:0]      tag1_comp_hist_vec,
    output index_comp_hist_t [`N_TABLES-1:0]    index_comp_hist_vec,

    input                   clk,
    input                   rst
);

logic                           fetch_pred_by_table;
logic [$clog2(`N_TABLES)-1:0]   provider_no;
logic                           fetch_pred;
logic                           fetch_alt_pred;

logic [`VADDR_WIDTH-1:0]            fetch_pc;
logic [`N_TABLES-1:0][`TAG_LENGTH-1:0]   fetch_tag_vec;


logic [`N_TABLES-1:0][`ENTRYS_LOG-1:0] fetch_index_vec;


logic [`VADDR_WIDTH-1:0]            update_pc;
logic [`N_TABLES-1:0][`TAG_LENGTH-1:0] update_tag_vec;
logic [`N_TABLES-1:0][`ENTRYS_LOG-1:0] update_index_vec;

logic [`N_TABLES-1:0]             predictions_vec;
logic [`N_TABLES-1:0]             tag_hit_vec;
logic [`N_TABLES-1:0]             fetch_is_new_entry_vec;
logic [`N_TABLES-1:0]             update_is_new_entry_vec;
logic [`N_TABLES-1:0][`USEFUL_BITS_LENGTH-1:0]  fetch_useful_bits_vec;
logic [`N_TABLES-1:0][`USEFUL_BITS_LENGTH-1:0]  update_useful_bits_vec;

// logic [`N_TABLES-1:0]             update_vec;
logic [`N_TABLES-1:0]             dec_u_vec;
logic [`N_TABLES-1:0]             alloc_vec;
logic [`USEFUL_BITS_LENGTH-1:0]   clear_useful_bit_mask;

logic                             alloc_en;
logic                           alt_win_inc, alt_win_dec;
logic [3:0]                     alt_win;
logic [`N_TABLES-1:0]           older_mask, free_mask, allocatable_mask, final_alloc_mask;
logic [$clog2(`N_TABLES)-1:0]   first_allocatable_entry, masked_entry, alloc_entry;

logic [$clog2(`N_TABLES)-1:0] alt_no;

logic [`BHT_IDX_WIDTH-1:0]  fetch_bht_index;
logic [`BHT_IDX_WIDTH-1:0]  update_bht_index;
logic                       bht_pred;

logic                     update_is_new_entry;

generate
    for (genvar i = 0; i < `N_TABLES; i++) begin
        tage_table tage_table_u 
        (
            // do prediction
            .fetch_en_i(fetch_valid_i),
            .fetch_index_i(fetch_index_vec[i]),
            .fetch_tag_i(fetch_tag_vec[i]),
            .fetch_pred_o(predictions_vec[i]),
            .fetch_tag_hit_o(tag_hit_vec[i]),
            .fetch_is_new_entry_o(fetch_is_new_entry_vec[i]),
            .fetch_useful_bits_o(fetch_useful_bits_vec[i]),

            // do update
            .update_en_i(update_pred_by_table_i && update_vaild_i && (provider_no_i == i)),
            .update_index_i(update_index_vec[i]),
            .update_tag_i(update_tag_vec[i]),
            .update_br_result_i(update_br_result_i),
            .update_pred_result_i(update_pred_i),
            .update_alt_pred_result_i(update_alt_pred_i),
            .update_dec_u_i(alloc_en && dec_u_vec[i]),
            .update_alloc_i(alloc_en && (alloc_entry == i)),
            .update_clear_ubit_mask(clear_useful_bit_mask),
            .update_useful_bits_o(update_useful_bits_vec[i]),
            .update_is_new_entry_o(update_is_new_entry_vec[i]),

            .clk(clk),
            .rst(rst)
        );
    end
endgenerate

bht bht_u
(
    // do prediction
    .fetch_index_i(fetch_bht_index),
    .prediction_o(bht_pred),

    // do update
    .update_en_i(update_vaild_i && (~update_pred_by_table_i)),
    .update_index_i(update_bht_index),
    .update_br_result_i(update_br_result_i), 

    .rst(rst),
    .clk(clk)
);

//-----------------------------------------------------------------------------------------------------------------

assign fetch_pc = (fetch_pc_i >> 1);
// hash to get tag
always @(*) begin
    for (int i=0; i<`N_TABLES; i++) begin
        fetch_tag_vec[i] = fetch_pc ^ tag0_comp_hist_vec[i].hist ^ (tag1_comp_hist_vec[i].hist << 1);
    end
end
// hash to get index
always @(*) begin
    for (int i=0; i<`N_TABLES; i++) begin
        fetch_index_vec[i] = get_idx(phist, index_comp_hist_vec[i].hist, fetch_pc, HIST_LEN[i], i);
    end
end
assign update_pc = (update_pc_i >> 1);
// hash to get tag
always @(*) begin
    for (int i=0; i<`N_TABLES; i++) begin
        update_tag_vec[i] = update_pc ^ tag0_comp_hist_vec[i].hist ^ (tag1_comp_hist_vec[i].hist << 1);
    end
end
// hash to get index
always @(*) begin
    for (int i=0; i<`N_TABLES; i++) begin
        update_index_vec[i] = get_idx(phist, index_comp_hist_vec[i].hist, update_pc, HIST_LEN[i], i);
    end
end

// hash to get bht index
assign fetch_bht_index = fetch_pc[`BHT_IDX_WIDTH-1:0];
assign update_bht_index = update_pc[`BHT_IDX_WIDTH-1:0];

//-----------------------------------------------------------------------------------------------------------------
// update ghist
assign next_ghist = (ghist << 1) | (update_br_result_i);
always @(posedge clk) begin
    if (rst) begin
        ghist <= 0;
    end
    else if (update_vaild_i) begin
        ghist <= next_ghist;
    end
end
// update phist
always @(posedge clk) begin
    if (rst) begin
        phist <= 0;
    end
    else if (update_vaild_i) begin
        phist <= (phist << 1) | update_pc[0];
    end
end
// update index comp hist
always @(posedge clk) begin
    if (rst) begin
        for (int i=0; i<`N_TABLES; i++) begin
            index_comp_hist_vec[i].hist <= 0;
            index_comp_hist_vec[i].ori_len <= HIST_LEN[i];
            index_comp_hist_vec[i].comp_len <= `ENTRYS_LOG;
            index_comp_hist_vec[i].outpoint <= HIST_LEN[i] % `ENTRYS_LOG;
        end
    end
    else if (update_vaild_i) begin
        for (int i=0; i<`N_TABLES; i++) begin
            index_comp_hist_vec[i].hist <= comp_hist(index_comp_hist_vec[i], next_ghist);
        end
    end
end
// update tag0 comp hist
always @(posedge clk) begin
    if (rst) begin
        for (int i=0; i<`N_TABLES; i++) begin
            tag0_comp_hist_vec[i].hist <= 0;
            tag0_comp_hist_vec[i].ori_len <= HIST_LEN[i];
            tag0_comp_hist_vec[i].comp_len <= TAG_LEN[i];
            tag0_comp_hist_vec[i].outpoint <= HIST_LEN[i] % TAG_LEN[i];
        end
    end
    else if (update_vaild_i) begin
        for (int i=0; i<`N_TABLES; i++) begin
            tag0_comp_hist_vec[i].hist <= comp_hist(tag0_comp_hist_vec[i], next_ghist);
        end
    end
end
// update tag1 comp hist
always @(posedge clk) begin
    if (rst) begin
        for (int i=0; i<`N_TABLES; i++) begin
            tag1_comp_hist_vec[i].hist <= 0;
            tag1_comp_hist_vec[i].ori_len <= HIST_LEN[i];
            tag1_comp_hist_vec[i].comp_len <= TAG_LEN[i] - 1;
            tag1_comp_hist_vec[i].outpoint <= HIST_LEN[i] % (TAG_LEN[i] - 1);
        end
    end
    else if (update_vaild_i) begin
        for (int i=0; i<`N_TABLES; i++) begin
            tag1_comp_hist_vec[i].hist <= comp_hist(tag1_comp_hist_vec[i], next_ghist);
        end
    end
end

//------------------------------------------------------------------------------------------------------------------
assign update_is_new_entry = (update_useful_bits_vec[provider_no_i] == 0);
always @(*) begin
    alloc_en = 0;
    alt_win_inc = 0;
    alt_win_dec = 0;
    if (update_vaild_i) begin
        alloc_en = (update_br_result_i != update_pred_i) && ~(provider_no_i == (`N_TABLES-1));  // the prediction is not given by the table with longest history
        if (update_pred_by_table_i) begin         // the prediction is given by table
            if (update_is_new_entry_vec[provider_no_i]) begin
                if (update_pred_i == update_br_result_i) begin
                    alloc_en = 0;
                end
                if (update_pred_i != update_alt_pred_i) begin
                    if (update_alt_pred_i == update_br_result_i) begin
                        alt_win_inc = alt_win == 4'b0111 ? 0 : 1;
                    end
                    else begin
                        alt_win_dec = alt_win == 4'b1111 ? 0 : 1;
                    end
                end
            end
        end
    end
end

always @(posedge clk) begin
    if (rst) begin
        alt_win <= 0;
    end
    else if (alt_win_inc) begin
        alt_win <= alt_win + 1'b1;
    end
    else if (alt_win_dec) begin
        alt_win <= alt_win - 1'b1;
    end
end

//------ select a new table for allocation ------
logic [6:0] lfsr;
lfsr lfsr_u(    
    .lfsr(lfsr),
    .en(1'b1),
    .clk(clk),
    .rst(rst)
);

always @(*) begin
    if (update_pred_by_table_i) begin
        for (int i=0; i<`N_TABLES; i++) begin
            older_mask[i] = (i > provider_no_i);
        end
    end
    else begin
        older_mask = {`N_TABLES{1'b1}};
    end
end

always @(*) begin
    for (int i=0; i<`N_TABLES; i++) begin
        free_mask[i] = (update_useful_bits_vec[i] == 0);
    end
end

assign allocatable_mask = older_mask & free_mask;

always @(*) begin
    first_allocatable_entry = 0;
    for (int i=`N_TABLES-1; i<=0; i--) begin
        if (allocatable_mask[i]) begin
            first_allocatable_entry = i;
        end
    end
end

assign final_alloc_mask = allocatable_mask & lfsr;

always @(*) begin
    masked_entry = 0;
    for (int i=`N_TABLES-1; i<=0; i--) begin
        if (final_alloc_mask[i]) begin
            masked_entry = i;
        end
    end
end

assign alloc_entry = (|final_alloc_mask) ? masked_entry : first_allocatable_entry;
assign dec_u_vec = (|allocatable_mask) ? 0 : older_mask;
always @(*) begin
    if ((|alloc_entry) & (|dec_u_vec)) begin
        $display("dec_u and alloc_entry can not set at one time!");
        $stop;
    end
end
//------ select a new table for allocation ------

logic [18:0] cnt;
always @(posedge clk) begin
    if (rst) begin
        clear_useful_bit_mask <= 0;
        cnt <= 0;
    end
    else if (update_vaild_i) begin
        if (&cnt[17:0]) begin
            if (cnt[18]) begin
                clear_useful_bit_mask <= 2'b10;
            end
            else begin
                clear_useful_bit_mask <= 2'b01;
            end
        end
        cnt <= cnt + 1;
    end
end

always @(*) begin
    provider_no = 0;
    alt_no = 0;
    fetch_pred_by_table = 0;
    for (int i=0; i<`N_TABLES; i++) begin
        if (tag_hit_vec[i]) begin
            alt_no = provider_no;
            provider_no = i;
            fetch_pred_by_table = 1;
        end
    end
end

//if the entry is recognized as a newly allocated entry and 
//counter alt_win is positive use the alternate prediction
assign fetch_alt_pred = (alt_no == 0) ? bht_pred : predictions_vec[alt_no];
always @(*) begin
    if (fetch_pred_by_table) begin
        if ((alt_win <= 0) || (~fetch_is_new_entry_vec[provider_no]) || (fetch_useful_bits_vec[provider_no] != 0)) begin
            fetch_pred = predictions_vec[provider_no];
        end
        else begin
            fetch_pred = fetch_alt_pred;
        end
    end
    else begin
        fetch_pred = fetch_alt_pred;
    end
end

always @(posedge clk) begin
    if (rst) begin
        fetch_pred_valid_o <= 0;
        fetch_pred_by_table_o <= 0;
        provider_no_o <= 0;
        fetch_pred_o <= 0;
        fetch_alt_pred_o <= 0;
    end
    else begin
        fetch_pred_valid_o <= fetch_valid_i;
        fetch_pred_by_table_o <= fetch_pred_by_table;
        provider_no_o <= provider_no;
        fetch_pred_o <= fetch_pred;
        fetch_alt_pred_o <= fetch_alt_pred;
    end
end


endmodule
