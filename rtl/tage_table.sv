`include "define.sv"
module tage_table
import typedef_pkg::*;
(
    // do prediction
    input logic                             fetch_en_i,
    input logic [`ENTRYS_LOG-1:0]            fetch_index_i,
    input logic [`TAG_LENGTH-1:0]            fetch_tag_i,
    output logic                            fetch_pred_o,
    output logic                            fetch_tag_hit_o,
    output logic                            fetch_is_new_entry_o,
    output logic [`USEFUL_BITS_LENGTH-1:0]  fetch_useful_bits_o,

    // do update
    input logic                             update_en_i,
    input logic [`ENTRYS_LOG-1:0]           update_index_i,
    input logic [`TAG_LENGTH-1:0]            update_tag_i,
    input logic                             update_br_result_i,
    input logic                             update_pred_result_i,
    input logic                             update_alt_pred_result_i,
    input logic                             update_dec_u_i,
    input logic                             update_alloc_i,
    input logic [`USEFUL_BITS_LENGTH-1:0]   update_clear_ubit_mask,

    output logic                            update_is_new_entry_o,
    output logic [`USEFUL_BITS_LENGTH-1:0]  update_useful_bits_o,

    input                   clk,
    input                   rst
);

logic [`CTR_LENGTH-1:0] fetch_ctr, update_ctr;

pred_table_entry_t [(1<<`ENTRYS_LOG)-1:0] entrys;

// do prediction
assign fetch_tag_hit_o = (entrys[fetch_index_i].tag == fetch_tag_i);
assign fetch_pred_o = entrys[fetch_index_i].ctr[`CTR_LENGTH-1];
assign fetch_ctr = entrys[fetch_index_i].ctr;
assign update_ctr = entrys[update_index_i].ctr;

assign fetch_useful_bits_o = entrys[fetch_index_i].useful_bits;
assign update_useful_bits_o = entrys[update_index_i].useful_bits;

assign fetch_is_new_entry_o = fetch_useful_bits_o == '0 && (fetch_ctr == {1'b1, {(`CTR_LENGTH - 1){1'b0}}} || fetch_ctr == {1'b0, {(`CTR_LENGTH - 1){1'b1}}});
assign update_is_new_entry_o = update_useful_bits_o == '0 && (update_ctr == {1'b1, {(`CTR_LENGTH - 1){1'b0}}} || update_ctr == {1'b0, {(`CTR_LENGTH - 1){1'b1}}});
always @(posedge clk) begin
    if (rst) begin
        for (int i = 0; i < (1<<`ENTRYS_LOG); i++) begin
            entrys[i].ctr <= 0;
            entrys[i].tag <= 0;
            entrys[i].useful_bits <= 0;
        end
    end
    else begin
        if (update_en_i) begin
            if (update_pred_result_i == update_br_result_i) begin
                if (~(&entrys[update_index_i].ctr)) begin
                    entrys[update_index_i].ctr <= entrys[update_index_i].ctr + 1'b1;
                end
            end
            else begin
                if (|entrys[update_index_i].ctr) begin
                    entrys[update_index_i].ctr <= entrys[update_index_i].ctr - 1'b1;
                end
            end

            if (update_pred_result_i != update_alt_pred_result_i) begin
                if (update_pred_result_i == update_br_result_i) begin
                    if (~(&entrys[update_index_i].useful_bits)) begin
                        entrys[update_index_i].useful_bits <= entrys[update_index_i].useful_bits + 1'b1;
                    end
                end
                else begin
                    if (|entrys[update_index_i].useful_bits) begin
                        entrys[update_index_i].useful_bits <= entrys[update_index_i].useful_bits - 1'b1;
                    end
                end
            end
        end

        if (update_dec_u_i) begin
            if (|entrys[update_index_i].useful_bits) begin
                entrys[update_index_i].useful_bits <= entrys[update_index_i].useful_bits - 1'b1;
            end
        end
        else if (update_alloc_i) begin
            entrys[update_index_i].ctr <= update_br_result_i ? {1'b1, {(`CTR_LENGTH - 1){1'b0}}} : {1'b0, {(`CTR_LENGTH - 1){1'b1}}};
            entrys[update_index_i].tag <= update_tag_i;
            entrys[update_index_i].useful_bits <= 0;
        end

        for (int i=0; i<(1<<`ENTRYS_LOG); i++) begin
            entrys[i].useful_bits = entrys[i].useful_bits & (~update_clear_ubit_mask);
        end
    end
end



endmodule
