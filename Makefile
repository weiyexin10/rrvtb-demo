CUR_MAKEFILE_PATH:=$(abspath $(lastword $(MAKEFILE_LIST)))
CUR_PATH:=$(patsubst %/,%, $(dir $(CUR_MAKEFILE_PATH)))
SRC_PATH:=$(CUR_PATH)/rtl
RTL_PATH:=$(CUR_PATH)/rtl

SIMULATOR_PATH:=$(CUR_PATH)/dromajo
SIMULATOR_INC:=$(SIMULATOR_PATH)/include
SIMULATOR_BUILD_PATH:=$(SIMULATOR_PATH)/build

RRVTB_PATH:=$(CUR_PATH)/rrvtb

vcs:rrvtb.so libdromajo_cosim.a
	vcs  +vcs+lic+wait +vcs+loopreport -sverilog  -kdb +vc -f flist $(DW_FILES) -top testbench +vpi testbench.cpp rrvtb.so libdromajo_cosim.a -load $(CUR_PATH)/rrvtb.so:tb_init_cb \
	+error+1 \
	+define+SIMULATION \
	+incdir+$(SRC_PATH) \
	+incdir+$(RRVTB_PATH) \
	+incdir+$(RTL_PATH) \
	+lint=TFIPC-L \
	-CFLAGS "-I$(SIMULATOR_INC) -I$(SIMULATOR_BUILD_PATH) -I$(ELFIO_PATH) -I$(RRVTB_PATH) -g -pthread -O3 -DGOLDMEM_INORDER" \
	-LDFLAGS "-lgmp -T$(RRVTB_PATH)/rrvtb.ld -g -pthread" \
	-debug_access+all -full64 +vpi

rrvtb.so:
	make -C $(RRVTB_PATH)
	cp $(RRVTB_PATH)/rrvtb.so ./

libdromajo_cosim.a:
	-mkdir $(SIMULATOR_BUILD_PATH)
	cd $(SIMULATOR_BUILD_PATH) && cmake -DCMAKE_BUILD_TYPE=Release .. && make -j48
	cp $(SIMULATOR_BUILD_PATH)/libdromajo_cosim.a ./

run: vcs
	./simv +image=$(ELF)

.PHONY : vcs clean 

clean:
	-rm -rf simv.daidir rrvtb.so simv csrc DVEfiles ucli.key vcdplus.vpd libdromajo_cosim.a dromajo/build
	-make -C $(SIMULATOR_BUILD_PATH) clean
	-make -C $(RRVTB_PATH) clean