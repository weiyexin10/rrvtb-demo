# Introduce
This demo is a simple project using RRVTB for verification. The RTL code to be verified is a TAGE branch predictor. We use an open source RISC-V model (Dromajo) to generate instruction streams and then stimulus to test the branch prediction accuracy of TAGE.
# Directory
    ├── README.md           // This file
    ├── dromajo             // The source code of Dromajo (an open source RISC-V model)
    ├── rtl                 // RTL code to be tested
    └── rrvtb               // Interface between C code and RTL code
    
# Usage

First of all, ensure that vcs has been installed and added to the environment variables.

To Compile: 

```
    make
```

Compile & Run:
```
    make run ELF=Path_to_elf_file
```
For example:
```
    make run ELF=./dhrystone.riscv
```