#ifndef __FRONTEND_H__
#define __FRONTEND_H__
#include "rrvtb/rrvtb.hpp"
#include "dromajo.hpp"
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <semaphore.h>
#include <pthread.h>
#include <unistd.h>
#include <map>

struct TageMeta
{
    bool        pred;
    bool        pred_by_table;
    uint64_t    provider_no;
    bool        alt_pred;
};

class Tage;
// static void advance_cb(void *);
// static void reset_cb(void *);
static void clock_gen_cb(void *);
static void clock_cb(void *, const vector<Bits> &);

static constexpr uint64_t catch_addr = 0x800018ea;

class Tage
{
public:
    Tage()
    // init SVObject here
    : rst("testbench.rst")
    , pc_i("testbench.pc_i")
    , pred_o("testbench.pred_o")
    , pred_by_table_o("testbench.pred_by_table_o")
    , provider_no_o("testbench.provider_no_o")
    , alt_pred_o("testbench.alt_pred_o")
    , update_valid_i("testbench.update_valid_i")
    , taken_i("testbench.taken_i")
    , pred_by_table_i("testbench.pred_by_table_i")
    , provider_no_i("testbench.provider_no_i")
    , alt_pred_i("testbench.alt_pred_i")
    {
        rst = 1;

        sem_init(&begin_mtx, 0, 0);
        sem_init(&finish_mtx, 0, 0);
        // register_timer_cb(clock_gen_cb, this, 1, true);
        register_signal_cb(clock_cb, this, {"testbench.clk"});
    }

    void Evaluate(uint64_t pc, bool update_valid, bool taken, TageMeta &meta)
    {
        pc_i = pc;
        update_valid_i = update_valid;
        taken_i = taken;
        pred_by_table_i = meta.pred_by_table;
        provider_no_i = meta.provider_no;
        alt_pred_i = meta.alt_pred;

        meta.alt_pred = alt_pred_o.value().to_ulong();
        meta.pred = pred_o.value().to_ulong();
        meta.pred_by_table = pred_by_table_o.value().to_ulong();
        meta.provider_no = provider_no_o.value().to_ulong();
    }


    void Advance()
    {
        int begin, finish;
        sem_getvalue(&begin_mtx, &begin);
        assert(begin == 0);
        sem_getvalue(&finish_mtx, &finish);
        assert(finish == 0);
        sem_post(&begin_mtx);
        sem_wait(&finish_mtx);
    }

    void Reset()
    {
        sem_wait(&finish_mtx);
        rst = 0;
        update_valid_i = 0;
    }

    friend void clock_cb(void *p, const vector<Bits> &sigs)
    {
        static bool running = false;
        static int last_sigs = 0;
        Tage *pTage = (Tage*)p;
        int newsigs = sigs[0] == 1 ? 1 : 0;
        if (sigs[0] == 1)
        {
            sem_post(&pTage->finish_mtx);
            sem_wait(&pTage->begin_mtx);
        }
        assert(!running || (running && last_sigs != newsigs));
        running = true;
        last_sigs = sigs[0] == 1 ? 1 : 0;
    }

    ~Tage();

private:
    sem_t           finish_mtx;
    sem_t           begin_mtx;

    SVObject rst;
    SVObject pc_i;
    SVObject pred_o;
    SVObject pred_by_table_o;
    SVObject provider_no_o;
    SVObject alt_pred_o;
    SVObject update_valid_i;
    SVObject taken_i;
    SVObject pred_by_table_i;
    SVObject provider_no_i;
    SVObject alt_pred_i;
};



#endif